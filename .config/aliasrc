#!/usr/bin/env sh

# Use neovim for vim if present.
command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d"

# Verbosity and settings that you pretty much just always are going to want.
alias \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -v" \
	mkd="mkdir -pv" \
	..="cd .." \
	...="cd ../.."
	yt="youtube-dl --add-metadata -i" \
	yta="yt -x -f bestaudio/best"

# Colorize commands when possible.
alias \
	grep="grep --color=auto" \
	ls="lsd -lAh --icon never --group-dirs first" \
	diff="diff --color=auto" \

# These common commands are just too long! Abbreviate them.
alias \
	trem="transmission-remote" \
	sdn="sudo shutdown -h now" \
	f="$FILE" \
	v="$EDITOR" \
	p="sudo pacman" \
	xi="sudo xbps-install" \
	xr="sudo xbps-remove" \
	xq="sudo xbps-query"

# Git aliases
alias \
	g="git" \
	gs="git status" \
	gc="git commit" \
	gp="git push" \
	dfg='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME' \
	dfgs='dfg status' \
	dfgc='dfg commit' \
	dfgp='dfg push'


# Some other stuff
alias \
	mpv="mpv --input-ipc-server=/tmp/mpvsoc$(date +%s)" \
	calcurse="calcurse -D ~/.config/calcurse" \
	x="sxiv -ft *" \
	ref="shortcuts >/dev/null; source ~/.config/shortcutrc" \
	usage="du -h -d1" \
