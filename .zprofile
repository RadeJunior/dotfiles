# Profile file. Runs on login. Environmental variables are set here.

[ -f "$HOME/.config/env" ] && source "$HOME/.config/env"

[ ! -f "$XDG_CONFIG_HOME/shortcuts" ] && shortcuts >/dev/null 2>&1

# Start graphical server on tty1 if not already running.
[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null && exec startx "$XINITRC"

# Switch escape and caps if tty and no passwd required:
sudo -n loadkeys "$XDG_DATA_HOME/ttymaps.kmap" 2>/dev/null
